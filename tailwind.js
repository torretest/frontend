module.exports = {
  important: true,
  theme: {
    container: {
      center: true,
    },
    fontFamily: {
      display: ["Gilroy", "sans-serif"],
      body: ["Graphik", "sans-serif"],
    },
    extend: {
      colors: {
        cyan: "#9cdbff",
        dark: {
          light: "#27292d",
          lighter: "#383b40",
          background: "#010101",
        },
        green: {
          torre: "#CDDC39",
          torreDark: "#A3B02D",
        },
      },
      margin: {
        96: "24rem",
        128: "32rem",
      },
    },
  },
  variants: {
    opacity: ["responsive", "hover"],
  },
  purge: ["./src/**/*.html", "./src/**/*.vue", "./src/**/*.jsx"],
};
