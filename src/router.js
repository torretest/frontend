import Vue from "vue";
import Router from "vue-router";
import Error from "@/views/Error/Error.vue";
import Home from "@/views/Home/Home.vue";
import Job from "@/views/Job/Job.vue";
import Search from "@/views/Search/Search.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
    },
    {
      path: "/search/:type",
      name: "search",
      component: Search,
    },
    {
      path: "/job/:id",
      name: "job",
      component: Job,
    },
    {
      path: "*",
      name: "error",
      component: Error,
    },
  ],
});
