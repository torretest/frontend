export default {
  name: "Home",
  computed: {
    getBackground() {
      return require("@/assets/home.png");
    },
  },
};
