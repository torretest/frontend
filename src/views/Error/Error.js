export default {
  name: "Error",
  computed: {
    getBackground() {
      return require("@/assets/404.svg");
    },
  },
};
