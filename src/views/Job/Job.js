import VueApexCharts from "vue-apexcharts";
import api from "@/core/api";
import PxPersonCard from "@/components/PersonCard/PxPersonCard.vue";
import numeral from "numeral";

export default {
  name: "Job",
  components: {
    PxPersonCard,
    VueApexCharts,
  },
  data() {
    return {
      isLoading: true,
      showTeam: false,
      showGraph: false,
      job: {},
      skills: {},
      teamMembers: [],
      chartOptions: {},
      series: [],
    };
  },
  created() {
    this.getJobDetail();
  },
  computed: {
    picture() {
      if (this.job.organizations == undefined) {
        return require("@/assets/logo.png");
      }
      if (this.job.organizations.length == 0) {
        return require("@/assets/logo.png");
      }
      return this.job.organizations[0].picture != null
        ? this.job.organizations[0].picture
        : require("@/assets/logo.png");
    },
    organizationsName() {
      if (this.job.organizations == undefined) {
        return "No name organizations";
      }
      if (this.job.organizations.length == 0) {
        return "No name organizations";
      }
      return this.job.organizations[0].name;
    },
    location() {
      if (this.job.locations == undefined) {
        return "Remote";
      }
      return this.job.locations.length === 0 ? "Remote" : this.job.locations[0];
    },
    compensation() {
      let compensation = this.job.compensation;
      if (!compensation) {
        return "Compensation: to be defined";
      }
      if (compensation.data == null) {
        return "Compensation: to be defined";
      }
      if (compensation.code == "FIXED") {
        // compensation = `${compensation.currency} ${Number(compensation.minAmount).toFixed(0)}`
        compensation = `${compensation.currency} ${numeral(
          compensation.minAmount
        ).format("(0a)")}`;
      } else {
        compensation = `${compensation.currency} ${numeral(
          compensation.minAmount
        ).format("(0a)")} - ${numeral(compensation.maxAmount).format(
          "(0a)"
        )} /${compensation.periodicity}`;
      }
      return compensation;
    },
    getBanner() {
      let atach = this.job.attachments;
      if (atach == undefined) {
        return require("@/assets/job-banner.svg");
      }
      if (atach.length == 0) {
        return require("@/assets/job-banner.svg");
      }
      return atach[0].address != null
        ? atach[0].address
        : require("@/assets/logo.png");
    },
    dataLineChar() {
      if (this.skills.testAvg == undefined) {
        return [];
      }
      return this.skills.testAvg.map((h) => [
        h.name,
        parseFloat(h.value).toFixed(2),
      ]);
    },
    agreement() {
      if (this.job.agreement == undefined) {
        return "";
      }
      return this.job.agreement.type.split("-").join(" ");
    },
  },
  methods: {
    getJobDetail() {
      let params = this.$route.params.id;
      params = params.split("-")[0];
      return api.getJobDetail(params).then((res) => {
        this.job = res;
        let arrayMembers = [];
        res.members.map((item) => {
          if (item.member && item.person.pictureThumbnail != undefined) {
            this.teamMembers.push(item.person);
          }
          arrayMembers.push(item.person.username);
        });
        if (this.teamMembers.length > 0) {
          this.showGraph = !this.showGraph;
          this.showTeam = !this.showTeam;
          this.getOrganizationSkills(arrayMembers);
        }
      });
    },
    getOrganizationSkills(members) {
      return api
        .getOrganizationSkills(members)
        .then((res) => {
          this.skills = res[0].data;
          let dataMyTeam = [],
            categories = [];
          this.skills.testAvg.map((trait) => {
            dataMyTeam.push(Number(trait.value).toFixed(2));
            categories.push(trait.name);
          });
          this.series.push({ name: "Team Avg", data: dataMyTeam });
          this.series.push({ name: "Media", data: this.skills.testMed });
          this.chartOptions = {
            chart: {
              height: 350,
              type: "line",
              dropShadow: {
                enabled: true,
                color: "#000",
                top: 18,
                left: 7,
                blur: 10,
                opacity: 0.2,
              },
              toolbar: {
                show: false,
              },
            },
            theme: {
              mode: "dark",
              palette: "palette2",
            },
            colors: ["#CDDC39", "#77B6EA"],
            dataLabels: {
              enabled: true,
            },
            stroke: {
              curve: "smooth",
            },
            title: {
              text: "Personality Traits",
              align: "left",
            },
            grid: {
              borderColor: "#A3B02D",
              row: {
                colors: ["#f1f1f1", "transparent"], // takes an array which will be repeated on columns
                opacity: 0.01,
              },
            },
            markers: {
              size: 1,
            },
            xaxis: {
              categories: categories,
              title: {
                text: "Traits",
              },
            },
            yaxis: {
              title: {
                text: "Avg",
              },
              min: 0,
              max: 4.5,
            },
            legend: {
              position: "top",
              horizontalAlign: "right",
              floating: true,
              offsetY: -25,
              offsetX: -5,
            },
          };
        })
        .finally(() => (this.isLoading = false));
    },
  },
};
