import PxFilter from "@/components/Filter/PxFilter.vue";
import PxCard from "@/components/Card/PxCard.vue";

import api from "@/core/api";

export default {
  name: "Search",
  components: {
    PxFilter,
    PxCard,
  },
  data() {
    return {
      isLoading: true,
      typeOfSearch: { abbre: "", url: "" },
      payloadToSend: "",
      offset: 0,
      size: 10,
      filters: {},
      results: {},
      showModal: false,
    };
  },
  created() {
    this.getPayloadToSend();
    this.getTypeOfSearchFilter("filter");
    if (this.payloadToSend !== "") {
      this.getTypeOfSearchFilter("data");
    }
  },
  watch: {
    $route() {
      this.getPayloadToSend();
      this.getTypeOfSearchFilter("data");
      this.getTypeOfSearchFilter("filter");
    },
  },
  methods: {
    modalToggle() {
      this.showModal = !this.showModal;
    },
    getTypeOfSearchFilter(type) {
      const nameOfUrl = this.$route.params.type,
        abbreOfUrl = nameOfUrl == "opportunities" ? "jobs" : "people";
      this.typeOfSearch = { abbre: abbreOfUrl, url: nameOfUrl };
      this.isLoading = true;
      const offset = type == "data" ? this.offset : 0,
        size = type == "data" ? this.size : 0,
        agregate = type == "data" ? false : true;
      return api
        .getDataByType(
          this.typeOfSearch.url,
          offset,
          size,
          agregate,
          this.payloadToSend
        )
        .then((res) => {
          if (type === "data") {
            this.results = res.results;
          } else {
            this.filters = res.aggregators;
          }
        })
        .finally(() => (this.isLoading = false));
    },
    getPayloadToSend() {
      let link = this.$route.query.q;
      let payload = "",
        payloadArray = [],
        payloadObject = { and: [] };
      if (link) {
        payload = decodeURIComponent(link);
        payload = payload.split("&");
        payload.map((item) => {
          const type = item.split(":")[0],
            term = item.split(":")[1];
          switch (type) {
            case "skill":
              payloadArray.push(
                `{"${type}": {"term": "${term}","experience": "1-plus-year"}}`
              );
              break;
            case "compensationrange": {
              let compensation = term.split(" ");
              let currency = compensation[0],
                min = compensation[1].split("/")[0].split("-")[0],
                max = compensation[1].split("/")[0].split("-")[1];
              payloadArray.push(
                `{"${type}": {"minAmount": "${min}","maxAmount": "${max}","currency":"${currency}","periodicity": "hourly"}}`
              );
              break;
            }
            case "type":
              payloadArray.push(`{"${type}": {"code": "${term}"}}`);
              break;
            case "status":
              payloadArray.push(`{"${type}": {"code": "${term}"}}`);
              break;
            default:
              payloadArray.push(`{"${type}": {"term": "${term}"}}`);
              break;
          }
        });
        if (payloadArray.length == 1) {
          payloadObject = payloadArray[0];
        } else {
          payloadObject = `{"and": [${payloadArray}]}`;
        }
      } else {
        payloadObject = "";
      }
      this.payloadToSend = payloadObject;
    },
  },
};
