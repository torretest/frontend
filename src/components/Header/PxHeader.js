export default {
  name: "PxHeader",
  data() {
    return {
      open: false,
    };
  },
  methods: {
    toggle() {
      this.open = !this.open;
    },
  },
  props: {
    links: {},
  },
};
