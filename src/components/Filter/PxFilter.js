export default {
  name: "PxFilter",
  props: {
    filters: {
      type: Object,
      default: () => [],
    },
    isLoading: {
      type: Boolean,
      default: true,
    },
    typeOfSearch: {
      type: String,
      default: "",
    },
  },
  watch: {
    filters(newValue) {
      //empty the array so we dont mix up people/job types
      this.filterName.splice(0);
      this.filterName = [
        { rawName: "status", labelName: "Status" },
        { rawName: "compensationrange", labelName: "Desired compensation" },
        { rawName: "type", labelName: "Open of job" },
        { rawName: "skill", labelName: "Top skill(s)" },
        { rawName: "opento", labelName: "Open to" },
        { rawName: "remoter", labelName: "Location" },
        { rawName: "organization", labelName: "Organization(s)" },
      ];

      let key = "";
      for (key in newValue) {
        //every type according to api
        this.filterName.map((element) => {
          if (element.rawName == key) {
            // if category exist we insert the data
            let filteredValues = [];
            newValue[key].map((item, index) => {
              if (index <= 9) {
                // so we only show at max 9 categories per type
                let link = this.$route.query.q,
                  proposedLink = encodeURIComponent(`${key}:${item.value}`);

                if (!link) {
                  // to check if ther is an active type filter
                  link = proposedLink; // no params then we put the first param
                } else {
                  if (!link.includes(proposedLink)) {
                    // else aggre to array payload
                    link = decodeURIComponent(link); // decod the first one then encode both together
                    link = encodeURIComponent(`${link}&${key}:${item.value}`);
                  }
                }

                item.link = {
                  name: "search",
                  params: {
                    type: `${
                      this.typeOfSearch == "jobs" ? "opportunities" : "people"
                    }`,
                  },
                  query: {
                    q: link,
                  },
                };
                filteredValues.push(item);
              }
            });
            this.$set(element, "dataArray", filteredValues);
          }
        });
      }
    },
  },
  data() {
    return {
      filterName: [],
    };
  },
  methods: {
    optClicked() {
      this.$emit("opt-clicked");
    },
  },
};
