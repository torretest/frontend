import numeral from "numeral";

export default {
  name: "PxCard",
  props: ["result"],
  computed: {
    link() {
      let slug = "";
      if (this.result.slug != null) {
        slug = `${this.result.slug}`;
      }
      const link = `${this.result.id}-${slug}`;
      return { name: "job", params: { id: link } };
    },
    picture() {
      if (this.result.organizations == undefined) {
        return require("@/assets/logo.png");
      }
      if (this.result.organizations.length == 0) {
        return require("@/assets/logo.png");
      }
      return this.result.organizations[0].picture != null
        ? this.result.organizations[0].picture
        : require("@/assets/logo.png");
    },
    type() {
      return this.result.type.split("-").join(" ");
    },
    organizationsName() {
      if (this.result.organizations == undefined) {
        return "No name organizations";
      }
      if (this.result.organizations.length == 0) {
        return "No name organizations";
      }
      return this.result.organizations[0].name;
    },
    location() {
      if (this.result.locations == undefined) {
        return "Remote";
      }
      return this.result.locations.length === 0
        ? "Remote"
        : this.result.locations[0];
    },
    compensation() {
      let compensation = this.result.compensation;
      if (!compensation) {
        return "Compensation: to be defined";
      }

      if (compensation.data == null) {
        return "Compensation: to be defined";
      }

      if (compensation.data.code == "FIXED") {
        // compensation = `${compensation.data.currency} ${Number(compensation.data.minAmount).toFixed(0)}`
        compensation = `${compensation.data.currency} ${numeral(
          compensation.data.minAmount
        ).format("(0a)")}`;
      } else {
        compensation = `${compensation.data.currency} ${numeral(
          compensation.data.minAmount
        ).format("(0a)")} - ${numeral(compensation.data.maxAmount).format(
          "(0a)"
        )} /${compensation.data.periodicity}`;
      }
      return compensation;
    },
    skills() {
      let skills = [];
      this.result.skills.map((skill, index) => {
        if (index <= 1) {
          skills.push(skill);
        }
      });
      return skills;
    },
  },
  methods: {
    newTab(link) {
      const routeData = this.$router.resolve(link);
      window.open(routeData.href, "_blank");
    },
  },
};
