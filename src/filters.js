import numeral from "numeral";

const dollarFilter = function (value) {
  if (!value) {
    return "0";
  }

  return numeral(value).format("(0a)");
};

const percentFilter = function (value) {
  if (!value) {
    return "0%";
  }

  return numeral(value).format(`${Number(value).toFixed(2)} %`);
};

const amountFilter = function (value) {
  if (!value) {
    return "0";
  }

  return numeral(value).format(`${Number(value)}a`);
};
export { dollarFilter, percentFilter, amountFilter };
