const searUrl = "https://search.torre.co/",
  jobUrl = "https://torre.co/api/opportunities/",
  biosUrl = "https://bio.torre.co/api/bios/",
  apiUrl =
    "https://www.vortipro.com/solucion/backend/torreBack/public/api/organization/strengths";

const getDataByType = async (type, offset, size, aggregate, payload) => {
  const rawData = await fetch(
    `${searUrl}${type}/_search/?offset=${offset}&size=${size}&aggregate=${aggregate}`,
    {
      method: "POST",
      headers: {
        "Access-Control-Allow-Origin": "*",
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: payload,
    }
  );
  const jsonData = await rawData.json();
  return jsonData;
};

const getJobDetail = async (id) => {
  const rawData = await fetch(`${jobUrl}${id}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  const jsonData = await rawData.json();
  return jsonData;
};

const getUserDetail = async (id) => {
  const rawData = await fetch(`${biosUrl}${id}`, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  const jsonData = await rawData.json();
  return jsonData;
};
const getOrganizationSkills = async (members) => {
  const rawData = await fetch(`${apiUrl}`, {
    method: "POST",
    mode: "cors",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ members: `${members}` }),
  });
  const jsonData = await rawData.json();
  return jsonData;
};

export default {
  getDataByType,
  getJobDetail,
  getUserDetail,
  getOrganizationSkills,
};
