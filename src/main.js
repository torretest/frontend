import Vue from "vue";
import App from "./App.vue";
import "@/assets/css/tailwind.css";
import Chartkick from "vue-chartkick";
import Chart from "chart.js";
import { VueSpinners } from "@saeris/vue-spinners";
import router from "@/router";
import { dollarFilter, percentFilter, amountFilter } from "@/filters";
import VueApexCharts from "vue-apexcharts";

Vue.use(VueSpinners);
Vue.use(Chartkick.use(Chart));
Vue.use(VueApexCharts);
Vue.filter("dollar", dollarFilter);
Vue.filter("percent", percentFilter);
Vue.filter("amount", amountFilter);
Vue.config.productionTip = false;
Vue.component("apexchart", VueApexCharts);

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
